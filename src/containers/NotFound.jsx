import React, { Fragment } from 'react'

const NotFound = () => (
    <Fragment>
        <h1>Pagina no encontrada</h1>
        <h2>Regresa al home</h2>
    </Fragment>    
)

export default NotFound